package tests;

import main.ParallelWordProcessor;
import main.dictProcedures.AnagrammGroupSearchProcedure;
import main.dictProcedures.AnagrammSearchProcedure;
import main.dictProcedures.CriteriaSearchProcedure;
import main.dictProcedures.DictProcedure;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class UnitTest {
    private static final Logger logger = LogManager.getLogger(UnitTest.class);
    private static final String in = "testWords.txt";

    @Test
    public void parallelTest() throws IOException {
        List<String> input = new LinkedList<>();
        CollectionUtils.addAll(input,"groovy", "yvoorg", "groovo", "vyroog", "gwe", "weq", "qre", "wert");
        Map<DictProcedure, Integer> procedures = new HashMap<>();
        procedures.put(new CriteriaSearchProcedure((wrd)->wrd.length()==3),4);

        List<String> correct = new LinkedList<>();
        CollectionUtils.addAll(correct,"3");
        genericParallelTest(input, procedures, correct);
    }

    private void genericParallelTest(List<String> words, Map<DictProcedure, Integer> procedures, List<String> correct) throws IOException {
        Files.write(Paths.get(in), words);

        ParallelWordProcessor processor = new ParallelWordProcessor(in, procedures);
        processor.process();

        Path outFile = Paths.get("CriteriaSearchProcedure.txt");
        List<String> output = Files.readAllLines(outFile);

        Assert.assertEquals("Not equal length of out and correct",output.size(), correct.size());
        Assert.assertTrue("Not equal out and correct",output.containsAll(correct));

        Files.delete(Paths.get(in));
    }

    @Test
    public void anagrammTest() {
        Assert.assertTrue(AnagrammSearchProcedure.isAnagramm("yvoorg", "groovy"));
        Assert.assertFalse(AnagrammSearchProcedure.isAnagramm("groovo", "groovy"));
    }

    @Test
    public void anagrammSearchTest() {
        List<String> input = new LinkedList<>();
        CollectionUtils.addAll(input,"groovy", "yvoorg", "groovo", "vyroog", "qwe", "weq", "qre", "wert");

        List<String> correct = new LinkedList<>();
        CollectionUtils.addAll(correct,"groovy", "yvoorg", "vyroog", "qwe", "weq");

        List<String> output = new AnagrammSearchProcedure().search(input);
        logger.trace(output);
        Assert.assertEquals(output.size(), correct.size());
        for (String word : output) {
            Assert.assertTrue(word, correct.contains(word));
        }
    }

    @Test
    public void reduceTest() {
        
    }

    @Test
    public void anagrammGroupSearchTest() {
        List<String> input = new LinkedList<>();
        CollectionUtils.addAll(input, "groovy", "yvoorg", "groovo", "vyroog", "qwe", "weq", "qre", "wert");

        List<String> correct = new LinkedList<>();
        CollectionUtils.addAll(correct, "groovy yvoorg vyroog", "qwe weq");

        List<String> output = new AnagrammGroupSearchProcedure().search(input);
        logger.trace(output);
        Assert.assertEquals(output.size(), correct.size());
        for (String word : output) {
            Assert.assertTrue(word, correct.contains(word));
        }
    }

    @Test
    public void criteriaSearchTest() {
        List<String> input = new LinkedList<>();
        CollectionUtils.addAll(input,"qwerty", "qwe", "","qweezcvc");

        List<String> correct = new LinkedList<>();
        CollectionUtils.addAll(correct,"2");

        List<String> output = new CriteriaSearchProcedure((word)->word.length()<=3).search(input);
        logger.trace(output);
        Assert.assertEquals("Incorrect criteria search",output.get(0), correct.get(0));
    }
}
