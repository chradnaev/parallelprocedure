package main;

import main.dictProcedures.DictProcedure;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;


public class ParallelWordProcessor {
    private final int threadNum = 4;
    private Map<DictProcedure, Integer> procedures;
    private List<String> words = new LinkedList<>();
    private Logger logger = LogManager.getLogger(ParallelWordProcessor.class);

    public ParallelWordProcessor(String in, Map<DictProcedure, Integer> dictProcMap) throws IOException {
        this.procedures = dictProcMap;
        try (BufferedReader reader = new BufferedReader(new FileReader(in) )) {
            String line = reader.readLine();
            while(line != null) {
                words.add(line);
                line = reader.readLine();
            }
        }
    }

    public void process() {
        ExecutorService service = Executors.newFixedThreadPool(threadNum);

        for (Map.Entry<DictProcedure, Integer>  entry: procedures.entrySet()) {
            DictProcedure procedure = entry.getKey();
            Integer partCount = entry.getValue();
            logger.trace("processing with "+procedure.getClass().getName());

            if (procedure.isJoinable()) {
                int sublistSize = words.size() / partCount;
                sublistSize = sublistSize <= 1 ? words.size() : sublistSize;
                AtomicInteger atomicPartCount = new AtomicInteger(partCount);

                int index = 0;
                List<String> results = new LinkedList<>();
                while (index < words.size()) {
                    List<String> sublist;
                    if (index + 2 * sublistSize > words.size()) {
                        sublist = words.subList(index, words.size());
                    } else {
                        sublist = words.subList(index,index + sublistSize);
                    }
                    service.execute(new ProcedureExecutor(sublist, procedure, results, atomicPartCount));
                    index += sublistSize;
                }

            }
            else {
                service.execute(new ProcedureExecutor(words, procedure, null, null));
            }
        }

        try {
            service.shutdown();
            service.awaitTermination(5, TimeUnit.SECONDS);
            logger.trace("All processes are ended");
        } catch (InterruptedException exc) {
            logger.error("Поток прерван: "+exc.getMessage());
            return;
        }
    }
}
