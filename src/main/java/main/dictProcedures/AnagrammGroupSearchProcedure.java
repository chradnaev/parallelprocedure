package main.dictProcedures;

import java.util.LinkedList;
import java.util.List;


public class AnagrammGroupSearchProcedure extends AnagrammSearchProcedure {

    @Override
    public List<String> search(List<String> words) {
        List<String> result = new LinkedList<>();
        boolean[] isMarked = new boolean[words.size()];

        for (int i = 0; i < words.size(); i++) {
            if (isMarked[i]) {
                continue;
            }
            StringBuilder anagrammList = new StringBuilder(words.get(i));
            for (int j = i+1; j < words.size(); j++) {
                if (isAnagramm(words.get(i),words.get(j))) {
                    anagrammList.append(" ").append(words.get(j));
                    isMarked[j] = true;
                }
            }
            if (!anagrammList.toString().equals(words.get(i))) {
                result.add(anagrammList.toString());
            }
        }
        return result;
    }
}
