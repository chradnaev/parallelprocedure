package main.dictProcedures;

import java.util.List;


public interface DictProcedure {
    List<String> search(List<String> words);
    default boolean isJoinable() {
        return false;
    }
    default void joinResults(List<String> results, List<String> singleResult) {
        throw new UnsupportedOperationException("Procedure is not joinable");
    }
}
