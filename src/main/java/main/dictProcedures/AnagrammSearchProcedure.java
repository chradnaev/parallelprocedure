package main.dictProcedures;

import java.util.*;

public class AnagrammSearchProcedure implements DictProcedure {
    @Override
    public List<String> search(List<String> words) {
        Set<String> result = new HashSet<>();
        for (int i = 0; i < words.size(); i++) {
            for (int j = i+1; j < words.size(); j++) {
                if (isAnagramm(words.get(i),words.get(j))) {
                    result.add(words.get(i));
                    result.add(words.get(j));
                }
            }
        }
        return new LinkedList<>(result);
    }
    public static boolean isAnagramm(String word1, String word2) {
        Map<Character,Integer> letters1 = letterCounts(word1);
        Map<Character,Integer> letters2 = letterCounts(word2);

        if (letters1.size() != letters2.size() || !letters1.keySet().containsAll(letters2.keySet())) {
            return false;
        }
        for (Map.Entry<Character, Integer> letterEntry : letters1.entrySet()) {
            if (!letterEntry.getValue().equals(letters2.get(letterEntry.getKey()))) {
                return false;
            }
        }
        return true;
    }
    public static Map<Character, Integer> letterCounts(String word) {
        Map<Character,Integer> letters = new HashMap<>();
        for (int i = 0; i < word.length(); i++) {
            if (letters.containsKey(word.charAt(i))) {
                letters.put(word.charAt(i),letters.get(word.charAt(i))+1);
            }
            else {
                letters.put(word.charAt(i),1);
            }
        }
        return letters;
    }
}
