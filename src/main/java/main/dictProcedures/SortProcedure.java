package main.dictProcedures;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class SortProcedure implements DictProcedure {
    @Override
    public List<String> search(List<String> words) {
        List<String> result = new LinkedList<>(words);
        Collections.sort(result);
        return result;
    }
}
