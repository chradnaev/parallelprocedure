package main.dictProcedures;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;


public class CriteriaSearchProcedure implements DictProcedure {
    private Predicate<String> wordPredicate;

    public CriteriaSearchProcedure(Predicate<String> wordPredicate) {
        this.wordPredicate = wordPredicate;
    }

    @Override
    public List<String> search(List<String> words) {
        long count = 0;
        for (String word : words) {
            if (wordPredicate.test(word)) {
                count++;
            }
        }
        List<String> result = new LinkedList<>();
        result.add(Long.toString(count));
        return result;
    }

    @Override
    public void joinResults(List<String> results, List<String> singleResult) {

        if (results.size() == 0) {
            results.add(singleResult.get(0));
        } else if (singleResult.size() == 0) {
            throw new IllegalArgumentException("singe result must be no empty");
        } else {
            int result = Integer.parseInt(results.get(0)) + Integer.parseInt(singleResult.get(0));
            results.set(0,String.valueOf(result));
        }
    }

    @Override
    public boolean isJoinable() {
        return true;
    }
}
