package main;

import main.dictProcedures.DictProcedure;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class ProcedureExecutor implements Runnable {
    private List<String> words;
    private DictProcedure procedure;
    private List<String> results;
    private AtomicInteger partCount;
    private Logger logger = LogManager.getLogger(ProcedureExecutor.class);

    public ProcedureExecutor(final List<String> words, DictProcedure procedure, List<String> results, AtomicInteger partCount) {
        this.words = words;
        this.procedure = procedure;
        this.results = results;
        this.partCount = partCount;
    }

    @Override
    public void run() {
        List<String> result = procedure.search(words);
        logger.trace(procedure.getClass().getSimpleName()+":"+words);

        if (procedure.isJoinable()) {
            synchronized (results) {
                procedure.joinResults(results, result);
            }
            partCount.addAndGet(-1);
        } else {
            results = result;
        }
        if ((procedure.isJoinable() && partCount.get() <= 1) || !procedure.isJoinable()) {
            writeToFile();
        }
    }

    private void writeToFile() {
        String filename = procedure.getClass().getSimpleName()+".txt";

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            for (String line : results) {
                writer.write(line+"\n");
            }
        } catch (IOException exc) {
            System.out.println(exc.toString());
        }

    }
}
