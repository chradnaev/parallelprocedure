package main;

import main.dictProcedures.*;
import java.util.HashMap;
import java.util.Map;


public class Main {
    /**
     *
     * @param args 1st - text file name, 2nd and further - comparator classes,
     *             'all' instead of 2nd arg - use all procedures
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            throw new IllegalArgumentException("Not enough arguments");
        }
        String in = args[0];
        Map<DictProcedure, Integer> procedures = new HashMap<>();

        if (args[1].matches("all:\\d+")) {
            int partCount = Integer.parseInt(args[1].split(":")[1]);

            procedures.put(new AnagrammGroupSearchProcedure(), partCount);
            procedures.put(new AnagrammSearchProcedure(), partCount);
            procedures.put(new CriteriaSearchProcedure((word)->word.contains("qw")), partCount);
            procedures.put(new SortProcedure(), partCount);
        } else {
            for (int i = 1; i < args.length; i++) {
                String dictClassName;
                int partCount = 1;

                String[] argPart = args[i].split(":");
                if (argPart.length == 2) {
                    dictClassName = argPart[0];
                    partCount = Integer.parseInt(argPart[1]);
                } else {
                    dictClassName = args[i];
                }

                Class<?> dictClass = Class.forName("main.dictProcedures."+dictClassName);
                if (dictClass.equals(CriteriaSearchProcedure.class)) {
                    procedures.put(new CriteriaSearchProcedure((word)->word.length()>=7),partCount);
                } else {
                    procedures.put((DictProcedure) dictClass.newInstance(),partCount);
                }
            }
        }
        System.out.println(procedures);

        new ParallelWordProcessor(in, procedures).process();
    }
}